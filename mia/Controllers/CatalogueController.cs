﻿using mia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace mia.Controllers
{
    [Authorize]
    public class CatalogueController : Controller
    {
        private miatradingEntities db = new miatradingEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(string query)
        {

            // must return all articles ordered from the client for making an another offer to client with the same products ordered.
            var articles = (from t in (from n in db.Line
                                       join Articles in db.Articles on n.Article_Id equals Articles.Id
                                       select new { n, Articles })
                            join Client in db.Clients on t.n.ClientNum equals Client.Id
                            where Client.Id.ToString().Equals(query)
                            group t by Client.ClientNum into g
                            select new {
                                code = g.Select(u => u.Articles).Distinct().GroupBy(u => u.FamilleArt),
                                lastPrice = g.Select(u => u.n).OrderByDescending(u => u.Date_Commande).Distinct().GroupBy(u => u.Article_Id)
                            });
            return Json(articles, JsonRequestBehavior.AllowGet);
        }

        // GET: Panier pour la gestion des offres
        public ActionResult Panier()
        {
            return View();
        }
    }
}
