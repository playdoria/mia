﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mia.Models;

namespace mia.Controllers
{
    [Authorize]
    public class ClientsController : Controller
    {
        private miatradingEntities db = new miatradingEntities();

        // GET: Clients
        public ActionResult Index()
        {
            return View();
        }
       

        [HttpPost]
        public JsonResult Index(string query)
        {
            var Clients = (from n in db.Line
                           join Client in db.Clients on n.ClientNum equals Client.Id
                           where (Client.ClientNum.ToString().Contains(query) || Client.ClientNom.Contains(query))
                           group n by Client.ClientNum into g
                           select new
                           {
                               ClientId = g.Key,
                               ClientName = db.Clients.Where(n => n.ClientNum == g.Key).Select(n => n.ClientNom),
                               ClientIdentifier = db.Clients.Where(n => n.ClientNum == g.Key).Select(n => n.Id)
                           });

            return Json(Clients, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Details(string query)
        {
            var Clients = (from t in (from n in db.Line
                                        join Articles in db.Articles on n.Article_Id equals Articles.Id
                                        select new { n, Articles })

                             join Client in db.Clients on t.n.ClientNum equals Client.Id
                             where Client.Id.ToString().Equals(query)
                             group t by Client.ClientNum into g
                             select new
                             {
                                 ClientId = g.Key,
                                 ClientName = db.Clients.Where(n => n.ClientNum == g.Key).Select(n => n.ClientNom),
                                 montantNet = g.Where(n => n.n.Date_Commande <= 20160101)
                                               .Sum(n => n.n.MontantNet),
                                 // CA par Familles
                                 nf2016 = g.Where(t => t.Articles.FamilleArt == 5 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 fs2016 = g.Where(t => t.Articles.FamilleArt == 6 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 the2016 = g.Where(t => t.Articles.FamilleArt == 7 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 ep2016 = g.Where(t => t.Articles.FamilleArt == 8 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 fp2016 = g.Where(t => t.Articles.FamilleArt == 9 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 hvs2016 = g.Where(t => t.Articles.FamilleArt == 10 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 dc2016 = g.Where(t => t.Articles.FamilleArt == 13 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 c2016 = g.Where(t => t.Articles.FamilleArt == 14 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 b2016 = g.Where(t => t.Articles.FamilleArt == 15 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 bsl2016 = g.Where(t => t.Articles.FamilleArt == 17 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 ls2016 = g.Where(t => t.Articles.FamilleArt == 20 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 oc2016 = g.Where(t => t.Articles.FamilleArt == 21 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 fsu2016 = g.Where(t => t.Articles.FamilleArt == 24 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),
                                 ff2016 = g.Where(t => t.Articles.FamilleArt == 25 && t.n.Date_Commande >= 20160101 && t.n.Date_Commande <= 20161230).Sum(t => t.n.MontantBrut),



                                 // CA par mois
                                 CAJANV_2016 = g.Where(n => n.n.Date_Commande >= 20160101 && n.n.Date_Commande <= 20160131)
                                              .Sum(n => n.n.MontantBrut),
                                 CAFEVR_2016 = g.Where(n => n.n.Date_Commande >= 20160201 && n.n.Date_Commande <= 20160231)
                                              .Sum(n => n.n.MontantBrut),
                                 CAMARS_2016 = g.Where(n => n.n.Date_Commande >= 20160301 && n.n.Date_Commande <= 20160331)
                                              .Sum(n => n.n.MontantBrut),
                                 CAAVRIL_2016 = g.Where(n => n.n.Date_Commande >= 20160401 && n.n.Date_Commande <= 20160431)
                                              .Sum(n => n.n.MontantBrut),
                                 CAMAI_2016 = g.Where(n => n.n.Date_Commande >= 20160501 && n.n.Date_Commande <= 20160531)
                                              .Sum(n => n.n.MontantBrut),
                                 CAJUIN_2016 = g.Where(n => n.n.Date_Commande >= 20160601 && n.n.Date_Commande <= 20160631)
                                              .Sum(n => n.n.MontantBrut),
                             });
            var Search = from t in db.ClientsLine where t.ClientNum.ToString().Equals(query) select t;
            return Json(new { Search, Clients }, JsonRequestBehavior.AllowGet);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            return View(clients);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ClientNum,ClientNom,Ville,Pays,CP")] Clients clients)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(clients);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clients);
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            return View(clients);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ClientNum,ClientNom,Ville,Pays,CP")] Clients clients)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clients).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(clients);
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clients clients = db.Clients.Find(id);
            if (clients == null)
            {
                return HttpNotFound();
            }
            return View(clients);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Clients clients = db.Clients.Find(id);
            db.Clients.Remove(clients);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
