﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mia.Controllers
{
    [Authorize]
    public class OffresController : Controller
    {
        // GET: Offres Index
        public ActionResult Index()
        {
            return View();
        }

        // GET: Panier pour la gestion des offres
        public ActionResult Panier()
        {
            return View();
        }
    }
}