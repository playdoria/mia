﻿using mia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mia.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private miatradingEntities db = new miatradingEntities();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(String Prefix) {
            miatradingEntities db = new miatradingEntities();
            List <Articles> art = db.Articles.ToList();

            var Articles = (from N in db.Articles
                            where (N.ArticleDesc.Contains(Prefix) || N.CodeArticle.Contains(Prefix))                        
                            select new { N.ArticleDesc, N.CodeArticle,
                                N.Id,
                                N.Score,
                                N.Collisage,
                                N.Type,
                                N.PrixIdeal, N.PrixLimiteBasse, N.PrixMoyen, N.PrixRevient, N.PrixDef,
                                N.MargeDef, N.MargeIdeal, N.MargeLimiteBasse, N.MargeMoyen});

            return Json(Articles, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetArticles ()
        {
            return View(db);

        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}