﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mia.Startup))]
namespace mia
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
