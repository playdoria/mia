﻿$(document).ready(function () {
    var idClient, hideplb = 0, hidepd = 0;
    // Paramètres pour Jquery-cookie
    $.cookie.json = true;
    // TODO : Refactorer ces var en array 2014, 2015, 2016
    var jan2015 = 0, fev2015 = 0, mar2015 = 0, avr2015 = 0, mai2015 = 0, jui2015 = 0, jul2015 = 0, aout2015 = 0, sep2015 = 0, oct2015 = 0, nov2015 = 0, dec2015 = 0;
    var jan2016 = 0, fev2016 = 0, mar2016 = 0, avr2016 = 0, mai2016 = 0, jui2016 = 0, jul2016 = 0, aout2016 = 0, sep2016 = 0, oct2016 = 0, nov2016 = 0, dec2016 = 0;
    var jan2014 = 0, fev2014 = 0, mar2014 = 0, avr2014 = 0, mai2014 = 0, jui2014 = 0, jul2014 = 0, aout2014 = 0, sep2014 = 0, oct2014 = 0, nov2014 = 0, dec2014 = 0;
    var articles = { items: [] };
    var isArt = 0, name, code, score, colisage = 0, prixRevient = 0, pr = 0, value, total, qte_commande;
    var catalogue;
    $("#ClientNom").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Clients/Index",
                type: "POST",
                dataType: "json",
                data: {
                    query: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            idClient: item['ClientIdentifier'],
                            label: item['ClientId'] + ' - ' + item['ClientName'],
                            value: item['ClientName']
                        }
                    }));
                }
            })
        },
        minLength: 1,
        messages: {
            noResults: "", results: "ok"
        },
        select: function (event, ui) {
            idClient = ui.item.idClient;
        },
    });
    function ifnull(elem) { // fonction qui retourne un zero au lieu du null.
        if (elem == null) {
            elem = 0;
        }
        return elem;
    }

    $("#btn-client").click(function () {
        var request = $.ajax({
            url: "/Clients/Details",
            type: "POST",
            dataType: "json",
            data: {
                query: idClient
            }
        });

        request.done(function (data) {

           
            notice("La fiche client à été correctement mis à jour", 1);
            // CA par mois
            console.log(data.Search);
            jan2014 = ifnull(data.Search[0].jan).toFixed(2)
            fev2014 = ifnull(data.Search[0].fev).toFixed(2)
            mar2014 = ifnull(data.Search[0].mar).toFixed(2)
            avr2014 = ifnull(data.Search[0].avr).toFixed(2)
            mai2014 = ifnull(data.Search[0].mai).toFixed(2)
            jui2014 = ifnull(data.Search[0].jui).toFixed(2)
            jul2014 = ifnull(data.Search[0].juil).toFixed(2)
            aout2014 = ifnull(data.Search[0].aou).toFixed(2)
            sep2014 = ifnull(data.Search[0].sep).toFixed(2)
            oct2014 = ifnull(data.Search[0].oct).toFixed(2)
            nov2014 = ifnull(data.Search[0].nov).toFixed(2)
            dec2014 = ifnull(data.Search[0].dec).toFixed(2)
            jan2015 = ifnull(data.Search[1].jan).toFixed(2)
            fev2015 = ifnull(data.Search[1].fev).toFixed(2)
            mar2015 = ifnull(data.Search[1].mar).toFixed(2)
            avr2015 = ifnull(data.Search[1].avr).toFixed(2)
            mai2015 = ifnull(data.Search[1].mai).toFixed(2)
            jui2015 = ifnull(data.Search[1].jui).toFixed(2)
            jul2015 = ifnull(data.Search[1].juil).toFixed(2)
            aout2015 = ifnull(data.Search[1].aou).toFixed(2)
            sep2015 = ifnull(data.Search[1].sep).toFixed(2)
            oct2015 = ifnull(data.Search[1].oct).toFixed(2)
            nov2015 = ifnull(data.Search[1].nov).toFixed(2)
            dec2015 = ifnull(data.Search[1].dec).toFixed(2)
            jan2016 = ifnull(data.Clients[0].CAJANV_2016).toFixed(2)
            fev2016 = ifnull(data.Clients[0].CAFEVR_2016).toFixed(2)
            mar2016 = ifnull(data.Clients[0].CAMARS_2016).toFixed(2)
            avr2016 = ifnull(data.Clients[0].CAAVRIL_2016).toFixed(2)
            mai2016 = ifnull(data.Clients[0].CAMAI_2016).toFixed(2)
            jui2016 = ifnull(data.Clients[0].CAJUIN_2016).toFixed(2)

            $('#chartMonth').remove(); // this is my <canvas> element
            $('#chart').append('<canvas id="chartMonth" width="200" height="200"><canvas>');
            MyLineChart();


            // CA par familles
            $('#fs2014').text(ifnull(data.Search[0].fruits_secs).toFixed(2) + ' €');
            $('#oc2014').text(ifnull(data.Search[0].olives_citrons).toFixed(2) + ' €');
            $('#ep2014').text(ifnull(data.Search[0].epices).toFixed(2) + ' €');
            $('#fsu2014').text(ifnull(data.Search[0].frais_surgeles).toFixed(2) + ' €');
            $('#dc2014').text(ifnull(data.Search[0].desserts_conf).toFixed(2) + ' €');
            $('#ls2014').text(ifnull(data.Search[0].legumes_secs).toFixed(2) + ' €');
            $('#bsl2014').text(ifnull(data.Search[0].bouillons).toFixed(2) + ' €');
            $('#fp2014').text(ifnull(data.Search[0].feculents).toFixed(2) + ' €');
            $('#c2014').text(ifnull(data.Search[0].conserves).toFixed(2) + ' €');
            $('#nf2014').text(ifnull(data.Search[0].nonfood).toFixed(2) + ' €');
            $('#b2014').text(ifnull(data.Search[0].boissons).toFixed(2) + ' €');
            $('#hvs2014').text(ifnull(data.Search[0].huile).toFixed(2) + ' €');
            $('#ff2014').text(ifnull(data.Search[0].fruits_frais).toFixed(2) + ' €');
            $('#fs2015').text(ifnull(data.Search[1].fruits_secs).toFixed(2) + ' €');
            $('#oc2015').text(ifnull(data.Search[1].olives_citrons).toFixed(2) + ' €');
            $('#ep2015').text(ifnull(data.Search[1].epices).toFixed(2) + ' €');
            $('#fsu2015').text(ifnull(data.Search[1].frais_surgeles).toFixed(2) + ' €');
            $('#dc2015').text(ifnull(data.Search[1].desserts_conf).toFixed(2) + ' €');
            $('#ls2015').text(ifnull(data.Search[1].legumes_secs).toFixed(2) + ' €');
            $('#bsl2015').text(ifnull(data.Search[1].bouillons).toFixed(2) + ' €');
            $('#fp2015').text(ifnull(data.Search[1].feculents).toFixed(2) + ' €');
            $('#c2015').text(ifnull(data.Search[1].conserves).toFixed(2) + ' €');
            $('#nf2015').text(ifnull(data.Search[1].nonfood).toFixed(2) + ' €');
            $('#b2015').text(ifnull(data.Search[1].boissons).toFixed(2) + ' €');
            $('#hvs2015').text(ifnull(data.Search[1].huile).toFixed(2) + ' €');
            $('#ff2015').text(ifnull(data.Search[1].fruits_frais).toFixed(2) + ' €');
            $('#fs2016').text(ifnull(data.Clients[0].fs2016).toFixed(2) + ' €');
            $('#oc2016').text(ifnull(data.Clients[0].oc2016).toFixed(2) + ' €');
            $('#ep2016').text(ifnull(data.Clients[0].ep2016).toFixed(2) + ' €');
            $('#fsu2016').text(ifnull(data.Clients[0].fsu2016).toFixed(2) + ' €');
            $('#dc2016').text(ifnull(data.Clients[0].dc2016).toFixed(2) + ' €');
            $('#ls2016').text(ifnull(data.Clients[0].ls2016).toFixed(2) + ' €');
            $('#bsl2016').text(ifnull(data.Clients[0].bsl2016).toFixed(2) + ' €');
            $('#fp2016').text(ifnull(data.Clients[0].fp2016).toFixed(2) + ' €');
            $('#c2016').text(ifnull(data.Clients[0].c2016).toFixed(2) + ' €');
            $('#nf2016').text(ifnull(data.Clients[0].nf2016).toFixed(2) + ' €');
            $('#b2016').text(ifnull(data.Clients[0].b2016).toFixed(2) + ' €');
            $('#hvs2016').text(ifnull(data.Clients[0].hvs2016).toFixed(2) + ' €');
            $('#ff2016').text(ifnull(data.Clients[0].ff2016).toFixed(2) + ' €');
        });
    })
    var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
    $("#btn-catalogue").click(function () {
        var request = $.ajax({
            url: "/Catalogue/Index",
            type: "POST",
            dataType: "json",
            data: {
                query: idClient
            }
        });
        request.done(function (data) {
            console.log(data);
            $('#catalogue').remove();
            $('#catalogue-client').append('<div id="catalogue"></div>');
            catalogue = data[0].code;
            lastPrice = data[0].lastPrice;
            notice("Le catalogue de <b>"+value+"</b> a bien été chargé", 1)
            for (var i = 0; i < catalogue.length; i++) {
                if (parseInt(catalogue[i][0].FamilleArt, 10) != 5) {
                    $('#catalogue').append('<div id="famille-desc">' + Famille(parseInt(catalogue[i][0].FamilleArt, 10)) + '</div>');
                    $('#catalogue').append('<div class="col-xs-3"><b>Description d\'article</b> </div>');
                    $('#catalogue').append('<div class="col-xs-1"><b>Code</b></div>');
                    $('#catalogue').append('<div class="col-xs-2"><b>Dernier prix</b></div>');
                    $('#catalogue').append('<div class="col-xs-2"><b>Quantité totale commandée</b></div>');
                    $('#catalogue').append('<div class="col-xs-2"><b>Quantité</b></div>');
                    $('#catalogue').append('<div class="col-xs-2"><b>Ajouter au panier</b></div>');

                }
                for (var u = 0; u < catalogue[i].length; u++) {
                    if (parseInt(catalogue[i][u].FamilleArt, 10) != 5) {
                        $('#catalogue').append('<div class="col-xs-3">' + catalogue[i][u].ArticleDesc + '</div>');
                        $('#catalogue').append('<div class="col-xs-1">' + catalogue[i][u].CodeArticle + '</div>');
                        for (var a = 0; a < lastPrice.length; a++) {
                            for (var o = 0; o < lastPrice[a].length; o++) {
                                if (o == 0 && catalogue[i][u].Id == lastPrice[a][o].Article_Id) {
                                    $('#catalogue').append('<div class="col-xs-2">' + lastPrice[a][o].PrixUnitaire + '</div>');
                                }
                                if (catalogue[i][u].Id == lastPrice[a][o].Article_Id) {
                                    qte_commande = lastPrice[a][o].QuantiteTotale === NaN ? 0 : lastPrice[a][o].QuantiteTotale
                                    console.log('quantite totale : ' + lastPrice[a][o].QuantiteTotale.type);
                                }
                            }
                        }
                        $('#catalogue').append('<div class="col-xs-2">' + qte_commande + '</div>');
                        $('#catalogue').append('<div class="col-xs-2"><button class="plus" type="button">+</button><input class="output" type="number" name="quantity" value="0" min="1" max="9999"><button class="moins" type="button">-</button></div>');
                        $('#catalogue').append('<div class="col-xs-2"><button type="button">Ajouter au panier</button></div>');
                    }
                }
            }
        
        });
    });
    $("#ArticleDesc").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Home/Index",
                type: "POST",
                dataType: "json",
                data: {
                    Prefix: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            idArt: item['Id'],
                            name: item['ArticleDesc'],
                            prixIdeal: item['PrixIdeal'],
                            margeIdeal: item['MargeIdeal'],
                            prixLimiteBasse: item['PrixLimiteBasse'],
                            margeLimiteBasse: item['MargeLimiteBasse'],
                            prixMoyen: item['PrixMoyen'],
                            margeMoyen: item['MargeMoyen'],
                            prixDef: item['PrixDef'],
                            margeDef: item['MargeDef'],
                            prixRevient: item['PrixRevient'],
                            code: item['CodeArticle'],
                            colisage: item['Collisage'],
                            type: item['Type'],
                            score: item['Score'],
                            label: item['CodeArticle'] + ' - ' + item['ArticleDesc'],
                            value: item['ArticleDesc']
                        }
                    }));


                }
            })
        },
        minLength: 3,
        select: function (event, ui) {
            console.log(ui.item.prixRevient),

            $('#idArticle').html('<a href="/Articles/Edit/' + ui.item.idArt + '">Editer l\'article</a>');
            name = ui.item.name;
            $('#code').text('Code: ' + ui.item.code);
            code = ui.item.code;
            $('#colisage').text('Colisage: ' + ui.item.colisage + ' ' + ui.item.type);
            value = ui.item.value;
            $('.colisage').text(' x ' + ui.item.colisage + ' ' + ui.item.type);
            colisage = ui.item.colisage;
            type = ui.item.type;
            score = ui.item.score;
            prixRevient = ui.item.prixRevient
            $('#prix_ideal').text(ifnull(ui.item.prixIdeal));
            $('#prix_moyen').text(ifnull(ui.item.prixMoyen));
            $('#prix_def').text(ifnull(ui.item.prixDef));
            $('#prix_limitebasse').text(ifnull(ui.item.prixLimiteBasse));
            $('#marge_ideal').text(ifnull(ui.item.margeIdeal.substr(2, 2)) + '%');
            $('#marge_moyen').text(ifnull(ui.item.margeMoyen.substr(2, 2)) + '%');
            $('#marge_def').text(ifnull(ui.item.margeDef.substr(2, 2)) + '%');
            $('#marge_limitebasse').text(ifnull(ui.item.margeLimiteBasse.substr(2, 2)) + '%');
            $(".ui-menu-item").hide();
            isArt = 1;
            notice("Le produit est désormais affiché", 2);
        },
        messages: {
            noResults: "", results: "ok"
        }
    })
        .keyup(function (e) {
        $(".ui-menu-item").hide();
        hideplb = 0;
        hidepd = 0;
        $("#hide-plb").hide();
        $("#hide-pd").hide();
    });
    $("#hide-plb").hide();
    $("#hide-pd").hide();
    $("#mortal").click(function () {
        if (hideplb == 0) {
            $("#hide-pd").show();
            hideplb++;
        } else {
            $("#hide-pd").hide();
            hideplb--;
        }
    });
    $("#kombat").click(function () {
        if (hidepd == 0) {
            $("#hide-plb").show();
            hidepd++;
        } else {
            $("#hide-plb").hide();
            hidepd--;
        }
    })
    $("#reset").click(function () {
        $('#ArticleDesc').val("");
        $('#ClientNom').val("");

    });
    //function Reset(id) {
    //    $("#reset").click(function () {
    //        $(id).val("");
    //    });
    //}
    //Reset("#")
    $("#chartMonth").hide();
    $("#ca-produits").hide();
    $("#notification").hide();
    $("#listing-gdo").hide();
    function MyLineChart() {
        var ctx = document.getElementById("chartMonth");
        var data = {
            labels: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
            datasets: [
              {
                  label: "2014",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(255,192,192,0.4)",
                  borderColor: "rgba(255,192,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(255,192,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(255,192,192,1)",
                  pointHoverBorderColor: "rgba(255,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [jan2014, fev2014, mar2014, avr2014, mai2014, jui2014, jul2014, aout2014, sep2014, oct2014, nov2014, dec2014]

              },
              {
                  label: "2015",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(75,192,192,0.4)",
                  borderColor: "rgba(75,192,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,192,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,192,192,1)",
                  pointHoverBorderColor: "rgba(75,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [jan2015, fev2015, mar2015, avr2015, mai2015, jui2015, jul2015, aout2015, sep2015, oct2015, nov2015, dec2015]
              },
              {
                  label: "2016",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(0,255,192,0.4)",
                  borderColor: "rgba(0,255,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(0,255,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(0,255,192,1)",
                  pointHoverBorderColor: "rgba(0,255,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [jan2016, fev2016, mar2016, avr2016, mai2016, jui2016]
              }
            ]
        }
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                maintainAspectRatio: true,
                scales: {
                    xAxes: [{
                        display: true
                    }]
                }
            }
        });
        $("#chartMonth").show();
        $("#ca-produits").show();
    }

    // Proposition d'offre (V3)
    $("#prix-offre").show();
    $('#notice').hide();
    $('.plus').click(function () {
        i = $(this).parent().find('.output').val();
        i++;
        if (i < 0) {
            i = 0;
        }
        $(this).parent().find('.output').val(i);
        return false;
    });
    $('.moins').click(function () {
        i = $(this).parent().find('.output').val();
        i--;
        if (i < 0) {
            i = 0;
        }
        $(this).parent().find('.output').val(i);
        return false;
    });
    $("#bouton-gdo").click(function () {
        if (hideplb == 0) {
            $("#prix-offre").hide();
            $("#listing-gdo").show();
            $("#bouton-gdo").text("Masquer");
            hideplb++;
        } else {
            $("#prix-offre").show();
            $("#listing-gdo").hide();
            $("#bouton-gdo").text("Details");
            hideplb--;
        }
    });
    $("#show-marge").click(function () {
        if (hideplb == 0) {
            $("#table-articles").find('.margetohide').hide();
            hideplb++;
        } else {
            $("#table-articles").find('.margetohide').show();
            hideplb--;
        }
    })
    $("#btn-valider").click(function () {
        if (!$('.selector-price').find(".output").val() || $('.selector-price').find(".output").val() < 1 || isArt === 0) {
            console.log($(".output").val());
            notice("les informations que vous avez entrés sont éronées, merci de les corriger", 0);
            return false;
        } else {
            articles.items.push({
                quantite: $('body').find('.selector-price').children('.small-box-footer').children('.input-group').children('.output').val(),
                nom: name,
                code: code,
                score: score,
                prixRevient: prixRevient,
                colisage: colisage,
                type: type,
                prix: $('body').find('.selector-price').children('.inner').children('p').text()
            });
            quantite: $('body').find('.small-box').children('.small-box-footer').children('.input-group').children('.output').val(0),
            $.cookie("articles-mia", articles);
            $('#details-elems').remove(); 
            $('#table-articles').append('<tbody id="details-elems"></tbody>');
            TableauViewFN();
        }
    })
    $('.small-box').click(function () {
        $('.small-box').removeClass('selector-price');
        $(this).addClass('selector-price');
    })
    $('.cbox').change(function () {
        if ($(this).prop('checked')) {
            $('.cbox').parent().parent().parent().removeClass('selector-price');
            $(this).parent().parent().parent().addClass('selector-price');
        } else {
            $(this).parent().parent().parent().removeClass('selector-price');
        }
    });
    $('#btn-export-excel').click(function () {
        console.log(articles);
        if (articles != null) {
            JSONToCSV(articles, "Offre de prix", true);
            return null;
        }
        notice("aucune liste n\'est enregistré", 0)
    })
    $('#btn-export-pdf').click(function () {
        if (articles != null) {
            makePdf();
            return null;
        }
        notice("aucune liste n\'est enregistré", 0);
    })

    function notice(texte, couleur) {
        // Color (int) : 
        // - 0 : red (problem)
        // - 1 : blue (info)
        // - 2 : green (validation)
        if (couleur == 0) {
            $('#notice').addClass('alert alert-danger');
            texte = "<b>Erreur : </b> " + texte
        } else if(couleur == 1) {
            $('#notice').addClass('alert alert-info');
            texte = "<b>Information : </b> " + texte
        } else {
            $('#notice').addClass('alert alert-success');
            texte = "<b>Validation: </b> " + texte
        }
        $('#notice').show();
        $('#notice').html(texte).delay(5000).fadeOut();
    }
    function addCommas(nStr) { // for sums
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function makePdf() {
        var doc = new jsPDF();
        doc.setFontSize(12);
        saut = 20
        doc.text(10, saut, 'Libellé');
        doc.text(120, saut, 'Code');
        doc.text(160, saut, 'Prix');
        doc.text(180, saut, 'Quantité');
        $.each(articles.items, function (i, index) {
            saut = saut + 10;
            doc.text(10, saut, index.nom)
            doc.text(120, saut, index.code)
            doc.text(160, saut, index.prix);
            doc.text(180, saut, index.quantite);
        });
        doc.line(25, 20, 60, 20); // horizontal line
        saut = saut + 20;
        doc.text(10, saut, "Total :")
        doc.text(180, saut, parseFloat(total).toFixed(2));
        doc.save('Offre.pdf');
    }
    function JSONToCSV(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        var CSV = '';
        //Set Report title in first row or line

        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";
            i = 0;
            //This loop will extract the label from 1st index of on array
            CSV += 'Libellé \t Code \t Prix \t Quantité \r\n'
            $.each(articles.items, function (i, index) {
                row = '';
                if (i == null) {

                } else {
                    row += index.nom + '\t';
                    row += index.code + '\t';
                    row += index.prix + '\t';
                    row += index.quantite + '\t';
                    
                }
                CSV += row + '\r\n';

                //add a line break after each row
            });
            CSV += 'Total : \t ' + total.toFixed(2) + '\r\n'
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            console.log(arrData.length);
            var row = "";
            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                alert(arrData[i]); 
                row += '"' + arrData[i][index] + '",';
            }
            row.slice(0, row.length - 1);
            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        var fileName = "OffreClient_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g, "_");

        //Initialize file format you want csv or xls
        var uri = 'data:text/xls;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension    

        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".xls";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    function CookieSeekerFN() {
        if ($.cookie("articles-mia")) { // Seek cookie and if cookie display the list.
            articles = $.cookie("articles-mia");
            TableauViewFN();
        }
    }
    function TableauViewFN() {
        console.log(articles);
        $("#items-gdo").text(articles.items.length);
        total = 0;
        prix_revient = 0;
        marge_total = 0;
        if (articles) {
            $.each(articles.items, function (i, item) {
                if (i != null) {
                    indice = i + 1;
                    prix_total = parseInt(item.quantite) * parseFloat(item.prix) * parseInt(item.colisage);
                    quantite = parseInt(item.quantite) * parseInt(item.colisage);
                    total += parseInt(item.quantite) * parseInt(item.colisage) * parseFloat(item.prix);
                    prix_revient_unite = parseInt(item.quantite) * parseFloat(item.prixRevient) * parseInt(item.colisage);
                    prix_revient += parseInt(item.quantite) * parseFloat(item.prixRevient) * parseInt(item.colisage);
                    marge = prix_total - prix_revient_unite;
                    marge_total = marge_total + marge;
                    $('<tr><td scope="row">' + indice + '</td> <td class="text-center">' + item.nom + '</td><td class="text-center hidden-sm hidden-xs">' + item.code + '</td> <td class="text-center">' + addCommas(parseFloat(item.prix).toFixed(2)) + ' €</td> <td class="text-center">' + quantite + ' ' + item.type.toLowerCase() + ' (' + parseInt(item.quantite) + ' * ' + item.colisage + ' ' + item.type.toLowerCase() + ') ' + '</td><td class="margetohide text-center">' + addCommas(marge.toFixed(2)) + ' €</td><td class="text-center">' + addCommas(prix_total.toFixed(2)) + ' €</td><td class="text-center"><button href="#" class="btn btn-xl btn-danger" onclick="DeleteJsonFN(' + i + ');return false;"><i class="fa fa-times-circle fa-3" aria-hidden="true"></i></button></td> </tr>').appendTo("#details-elems");
                }
            });
        }
        total_moins_marge = total - marge_total;
        $('<tr><td scope="row"><b></b></td><td id="total" class="text-center"><b>Total</b></td><td class="hidden-sm hidden-xs"></td> <td></td><td></td><td class="margetohide text-center"><b>' + marge_total.toFixed(2) + ' €</b></td><td class="text-center"><b>' + addCommas(total.toFixed(2)) + '  €</b></td><td class="text-center"><b class="margetohide">' + addCommas(total_moins_marge.toFixed(2)) + '</b></td> </tr>').appendTo("#details-elems");
        if (hideplb == 0) {
            $("#table-articles").find('.margetohide').hide();
            hideplb++;
        } else {
            $("#table-articles").find('.margetohide').show();
            hideplb--;
        }
    }
    DeleteJsonFN = function (intjson) {
        $('#details-elems').remove();
        $('#table-articles').append('<tbody id="details-elems"></tbody>');
        console.log(articles.items[intjson]);
        articles.items.splice(intjson, 1);
        console.log(articles);
        $.removeCookie("articles-mia");
        $.cookie("articles-mia", articles);
        $("#items-gdo").text(articles.items.length);
        TableauViewFN()
        notice("L'élément à bien été supprimé", 2);
        //CookieSeekerFN();
    }
    CookieSeekerFN();
    $('.margetohide').hide();

    // V4 : Catalogue d'achat de produits par les clients

    Famille = function (familleNb) {
        switch (familleNb) {
            case 5:
                return "non food";
                break;
            case 6:
                return "Fruits secs";
                break;
            case 7:
                return "Thes";
                break;
            case 8:
                return "Epices";
                break;
            case 9:
                return "Feculents et pates";
                break;
            case 10:
                return "Huile, vinaigre et sauce"
                break;
            case 13:
                return "Desserts et confisseries";
                break;
            case 14:
                return "Conserves";
                break;
            case 15:
                return "Boissons";
                break;
            case 17:
                return "Bouillon, soupe et levure";
                break;
            case 20:
                return "Legumes secs";
                break;
            case 21:
                return "Olives et citrons";
                break;
            case 24:
                return "Frais et surgelés";
                break;
            case 25:
                return "Fruits frais";
                break;
        }
    }
});